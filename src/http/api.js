import {http} from './index'

//登录方法
export function login(data) {
    return http('login','POST',data)
}
//获取权限列表
export function getMenus() {
    return http('menus','GET')
}
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path:"/",
    redirect: "/login"
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "Login" */ '../views/Login.vue')
  },
  {
    path: '/index',
    name: 'index',
    component: () => import(/* webpackChunkName: "index" */ '../views/index.vue'),
    children:[
      {
        path: 'roles',
        name: 'roles',
        component: () => import(/* webpackChunkName: "roles" */ '../coment/roles/Roles.vue')
      },
      {
        path: 'users',
        name: 'users',
        component: () => import(/* webpackChunkName: "users" */ '../coment/users/Users.vue')
      },
      {
        path: 'rights',
        name: 'rights',
        component: () => import(/* webpackChunkName: "rights" */ '../coment/rights/Rights.vue')
      },
      {
        path: 'goods',
        name: 'goods',
        component: () => import(/* webpackChunkName: "goods" */ '../coment/goods/Goods.vue')
      },
      {
        path: 'params',
        name: 'params',
        component: () => import(/* webpackChunkName: "params" */ '../coment/params/Params.vue')
      },
      {
        path: 'categories',
        name: 'categories',
        component: () => import(/* webpackChunkName: "categories" */ '../coment/categories/Categories.vue')
      },
      {
        path: 'orders',
        name: 'orders',
        component: () => import(/* webpackChunkName: "orders" */ '../coment/orders/Orders.vue')
      },
      {
        path: 'reports',
        name: 'reports',
        component: () => import(/* webpackChunkName: "reports" */ '../coment/reports/Reports.vue')
      },
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
